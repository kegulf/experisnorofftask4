**Experis Academy, Norway**

**Authors:**
* **Odd Martin Hansen**

# Task 4: Nested Rectangle
Write a program which can be used print a rectangle of size chosen by the user at run-time.
* The rectangle should have an outer edge and a second inner edge.
* There must be one space between in the inner and outer edge. 

The rectangle can be made of any character that you choose. (# is probably a good choice)

You may choose the orientation yourself. 

The file must compile without errors. example: (after compilation)

<img src="https://gitlab.com/kegulf/experisnorofftask4/raw/master/nested_rectangles.png" alt="Image of the nested rectangles" />
