﻿using System;


/*
 * Write a program which can be used print a rectangle of size chosen by the user at run-time.
 * The rectangle should have an outer edge and a second inner edge.
 * There must be one space between in the inner and outer edge. The rectangle can be made of any character that you
 * choose. (# is probably a good choice)
 * You may choose the orientation yourself. The file must compile without errors. example: (after compilation)
 * I use _ to show a space here (Yours should be blank)
 * #######
 * #     #
 * # ### #
 * # # # #
 * # # # #
 * # ### #
 * #     #
 * ####### 
 */
namespace ExperisNoroffTask4 {
    class Program {

        static void Main(string[] args) {

            // Experimenting with tuples, and it worked <3
            var (height, width) = RequestSquareDimensionsFromUser();
            Console.WriteLine(CreateDoubleBorderedRectangle(height, width) );

        }



        /// <summary>
        /// Creates a string representing a double bordered rectangle with 
        /// one space between the inner and outer border.
        /// </summary>
        /// <param name="height">The height of the rectangle</param>
        /// <param name="width">The width of the rectangle</param>
        /// <returns></returns>
        static string CreateDoubleBorderedRectangle(int height, int width) {
            
            string squares = "";

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {

                    bool coordinateIsOnABorder = CoordinateIsOnABorder(i, j, height, width);

                    squares += (coordinateIsOnABorder) ? "#" : " ";
                    squares += (j == width - 1) ? "\n" : "";
                }
            }

            return squares;
        }



        /// <summary>
        /// Checks if the (i, j) pair is on the inner or outer borders of the rectangle.
        /// Returns true if it is, false if not
        /// </summary>
        /// <param name="i">Outer loop counter</param>
        /// <param name="j">Inner loop counter</param>
        /// <param name="height">Height of outer rectangle</param>
        /// <param name="width">Width of outer rectangle</param>
        /// <returns>A bool that is true if the coord is on one of the borders, false if not.</returns>
        static bool CoordinateIsOnABorder(int i, int j, int height, int width) {
            return i == 0 || j == 0 || i == height - 1 || j == width - 1     // Outer Square
                || ((i == 2 || i == height - 3) && j >= 2 && j < width - 2)  // Inner square, horizontal lines
                || ((j == 2 || j == width - 3) && i >= 2 && i < height - 2); // Inner square, vertical lines
        }



        /// <summary>
        /// Asks user for height and width of the outer square,
        /// and returns the input values as a tuple of ints (height, width)
        /// </summary>
        /// <returns>A int tuple containing height and width</returns>
        static (int, int) RequestSquareDimensionsFromUser() {

            int width = 0, height = 0;

            // In case the user is less gifted and enters something else than a number
            try {
                Console.WriteLine("\nWhat height should the outer square be?");
                height = int.Parse(Console.ReadLine());

                Console.WriteLine("\nWhat width should the outer sqare be ");
                width = int.Parse(Console.ReadLine());
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }

            if (height < 5 || width < 5 ) {
                Console.WriteLine("\nThe width and height must be at least 5, and valid numbers");
                RequestSquareDimensionsFromUser();
            }

            return (height, width);
        }
    }
}
